libdata-walk-perl (2.01-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 13 Jun 2022 07:49:09 +0100

libdata-walk-perl (2.01-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Nick Morrott ]
  * Imported Upstream version 2.01
  * Update copyright years
  * Bump Standards-Version to 3.9.8 (no changes)
  * Add debian/upstream/metadata
  * Refresh 01-drop_shebang_from_pm.patch
  * Add 02-spelling_error_in_manpage.patch
  * Fix typo in long description
  * Add debian/NEWS (2.x incompatible with 1.x releases)

  [ gregor herrmann ]
  * Add versioned (build) dependency on Scalar::Util 1.38.
  * Make short description a noun phrase.

 -- Nick Morrott <knowledgejunkie@gmail.com>  Tue, 02 Aug 2016 19:51:43 +0100

libdata-walk-perl (1.00-3) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Add explicit build dependency on libmodule-build-perl.
  * debian/copyright: switch formatting to Copyright-Format 1.0.
  * Install upstream NEWS as changelog, drop README.
  * Drop ancient debian/README.source referring to quilt.
  * Fix typo in long description.

 -- gregor herrmann <gregoa@debian.org>  Sun, 07 Jun 2015 17:31:12 +0200

libdata-walk-perl (1.00-2) unstable; urgency=low

  * Team upload

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed:
    Homepage pseudo-field (Description); XS-Vcs-Svn fields.
    (Closes: #615415).
  * Refresh debian/rules, no functional changes, except: don't create
    .packlist file any more.
  * debian/watch: use dist-based URL.
  * Set Standards-Version to 3.8.0; add debian/README.source to document
    quilt usage.
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * Fix the following lintian warnings:
    + package-has-a-duplicate-build-relation
    + copyright-with-old-dh-make-debian-copyright
    + quilt-patch-missing-description
  * Switch to source format "3.0 (quilt)"
    + Remove quilt build-dependency and quilt traces from debian/rules
  * Bump debhelper compatibility to 9
    + Update versioned debhelper build-dependency accordingly
  * Revamp debian/rules
    + Fix lintian warning debian-rules-missing-recommended-target
    + Replace "dh_clean -k" with "dh_prep"
    + Use dh_auto_{configure,build,test,install,clean}
    + Remove obsolete dh_{clean,installchangelogs} parameter
    + Move dh_installdocs parameters to debian/docs
    + Remove obsolete variable usage
    + Finally switch to a minimal dh-style debian/rules file
  * Bump Standards-Version to 3.9.5 (no further changes)

 -- Axel Beckert <abe@debian.org>  Wed, 25 Dec 2013 21:42:01 +0100

libdata-walk-perl (1.00-1) unstable; urgency=low

  * Initial Release (Closes: #442837).

 -- Damyan Ivanov <dmn@debian.org>  Mon, 17 Sep 2007 13:33:07 +0300
